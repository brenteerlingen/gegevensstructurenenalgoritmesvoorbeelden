package Elementary_Sort;

import Sort.Sort;

public class InsertionSort extends Sort {

    @Override
    public void sort(Comparable[] a) {
        int n = a.length;
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j > 0 && less(a[j], a[j-1]); j--){
                exchange(a, j, j-1);
            }
        }
    }
}
