import Elementary_Sort.InsertionSort;
import Elementary_Sort.SelectionSort;
import Elementary_Sort.ShellSort;
import Merge_Sort.BottomUpMergeSort;
import Merge_Sort.TopDownMergeSort;
import Quicksort.Quicksort;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[] a = readTinyFile();

        SelectionSort selecSort = new SelectionSort();
        selecSort.sort(a);
        assert selecSort.isSorted(a);
        System.out.println("Selection sort: ");
        selecSort.show(a);

        a = readTinyFile();

        System.out.println();

        InsertionSort insertSort = new InsertionSort();
        insertSort.sort(a);
        assert insertSort.isSorted(a);
        System.out.println("Insertion sort: ");
        insertSort.show(a);

        a = readTinyFile();

        System.out.println();

        ShellSort shellSort = new ShellSort();
        shellSort.sort(a);
        assert shellSort.isSorted(a);
        System.out.println("Shellsort: ");
        shellSort.show(a);

        a = readTinyFile();

        System.out.println();

        TopDownMergeSort topDownMergeSort = new TopDownMergeSort();
        topDownMergeSort.sort(a);
        assert topDownMergeSort.isSorted(a);
        System.out.println("Top-down mergesort: ");
        topDownMergeSort.show(a);

        a = readTinyFile();
        System.out.println();

        BottomUpMergeSort bottomUpMergeSort = new BottomUpMergeSort();
        bottomUpMergeSort.sort(a);
        assert bottomUpMergeSort.isSorted(a);
        System.out.println("Bottom-up mergesort: ");
        bottomUpMergeSort.show(a);

        a = readTinyFile();
        System.out.println();

        Quicksort quicksort = new Quicksort();
        quicksort.sort(a);
        assert quicksort.isSorted(a);
        System.out.println("Quicksort: ");
        quicksort.show(a);
    }

    private static String[] readTinyFile(){
        String[] a = new String[0];

        File file =
                new File("src/tiny.txt");
        Scanner sc = null;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (sc.hasNextLine())
            a = sc.nextLine().split( " ");

        return a;
    }
}
