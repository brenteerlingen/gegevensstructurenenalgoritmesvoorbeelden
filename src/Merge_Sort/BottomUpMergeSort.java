package Merge_Sort;

import Sort.Sort;

public class BottomUpMergeSort extends Sort {

    private static Comparable[] aux;

    private void merge(Comparable[] a, int lo, int  hi, int mid){
        int i = lo;
        int j = mid+1;
        for(int k = lo; k <= hi; k++)
            aux[k] = a[k];

        for(int k = lo; k<=hi; k++){
            if(i > mid)
                a[k] = aux[j++];
            else if(j > hi)
                a[k] = aux[i++];
            else if(less(aux[j], aux[i]))
                a[k] = aux[j++];
            else
                a[k] = aux[i++];
        }
    }

    @Override
    public void sort(Comparable[] a) {
        int n = a.length;
        aux = new Comparable[n];
        for(int len = 1; len < n; len++)
            for(int lo = 0; lo < n-len; lo += len + len)
                merge(a, lo, lo+len-1, Math.min(lo+len+len-1, n-1));
    }
}
