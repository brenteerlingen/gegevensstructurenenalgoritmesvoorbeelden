import Elementary_Sort.InsertionSort;
import Elementary_Sort.SelectionSort;
import Elementary_Sort.ShellSort;
import org.apache.commons.lang3.time.StopWatch;

public class SortCompare {
    public static double time(String alg, Double[] a) {
        StopWatch timer = new StopWatch();

        timer.start();

        if(alg.equals("Insertion"))
            new InsertionSort().sort(a);
        if(alg.equals("Selection"))
            new SelectionSort().sort(a);
        if(alg.equals("Shell"))
            new ShellSort().sort(a);

        return timer.getTime();
    }

    public static double timeRandomInput(String alg, int n, int trails){
        double total = 0.0;
        Double[] a = fillDoublesArray(n);

        for(int t = 0; t < trails; t++){
            for(int i = 0; i < n; i++)
                a[i] = Math.random() * 1.0;
            total += time(alg, a);
        }
        return total;
    }

    public static Double[] fillDoublesArray(int n){
        Double[] a = new Double[n];

        for (int i = 0; i < n; i++){
            a[i] = Math.random() * 50;
        }

        return a;
    }

    public static void main(String[] args) {
        String alg1 = args[0];
        String alg2 = args[1];
        int n = Integer.parseInt(args[2]);
        int trails = Integer.parseInt(args[3]);
        double time1 = timeRandomInput(alg1, n, trails);
        double time2 = timeRandomInput(alg2, n, trails);
        double ratio = time2 / time1;
        System.out.printf("For %d random Doubles\n  %s is", n, alg1);
        System.out.printf(" %.1f times faster than %s\n", ratio, alg2);
    }
}
